﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompetitionManager
{
    class Skill
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public ICollection<CompetitorSkill> competitorSkills { get; set; }

        public Skill()
        {

        }
    }
}
