﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompetitionManager
{
    class CompetitorSkill
    {
        public int CompetitorId { get; set; }
        public Competitor Competitor { get; set; }
        public int SkillId { get; set; }
        public Skill Skill { get; set; }
    }
}
