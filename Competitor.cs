﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompetitionManager
{
    class Competitor
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Nationality { get; set; }
        public int Score { get; set; }
        public int TotalStroke { get; set; }
        public Coach Coach { get; set; }
        public int? SponsorId { get; set; }
        public Sponsor Sponsor { get; set; }
        public ICollection<CompetitorSkill> competitorSkills { get; set; }

        public Competitor()
        {

        }

    }
}
