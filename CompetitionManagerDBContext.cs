﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace CompetitionManager
{
    class CompetitionManagerDBContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=PC7381;Database=Competition Manager;Trusted_Connection=True;MultipleActiveResultSets=true;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CompetitorSkill>().HasKey(cs => new { cs.CompetitorId, cs.SkillId });
        }

        public DbSet<Sponsor> Sponsors { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<Competitor> Competitors { get; set; }
        public DbSet<Coach> Coaches { get; set; }
        public DbSet<CompetitorSkill> CompetitorSkills { get; set; }
    }
}
