﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompetitionManager
{
    class Sponsor
    {
        public int Id { get; set; }
        public string Brand { get; set; }
        public double AnualPayLoad { get; set; }
        public ICollection<Competitor> Competitors { get; set; }

        public Sponsor()
        {

        }
    }
}
