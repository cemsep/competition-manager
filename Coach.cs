﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompetitionManager
{
    class Coach
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? CompetitorId { get; set; }
        public Competitor Competitor { get; set; }

        public Coach()
        {

        }
    }
}
