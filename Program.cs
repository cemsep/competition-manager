﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CompetitionManager
{
    class Program
    {
        static void Main(string[] args)
        {
            SeedSponsors();
            SeedSkills();
            SeedCompetitors();
            SeedChoaches();
            SeedCompetitorSkills();
            EagerLoading();
            ReadCompetitors();
        }

        public static void SeedCompetitors()
        {
            Console.WriteLine("Seeding competitors...");

            using (CompetitionManagerDBContext context = new CompetitionManagerDBContext()) 
            {
                context.Competitors.Add(new Competitor()
                {
                    FirstName = "Tiger",
                    LastName = "Woods",
                    Nationality = "USA",
                    Score = -6,
                    TotalStroke = 68,
                    SponsorId = 1
                });
                context.Competitors.Add(new Competitor()
                {
                    FirstName = "Jason",
                    LastName = "Day",
                    Nationality = "AUS",
                    Score = -7,
                    TotalStroke = 67,
                    SponsorId = 3
                });
                context.Competitors.Add(new Competitor()
                {
                    FirstName = "Ian",
                    LastName = "Poulter",
                    Nationality = "ENG",
                    Score = -5,
                    TotalStroke = 71,
                    SponsorId = 4
                });
                context.Competitors.Add(new Competitor()
                {
                    FirstName = "Jon",
                    LastName = "Rahm",
                    Nationality = "ESP",
                    Score = -5,
                    TotalStroke = 70,
                    SponsorId = 2
                });
                context.Competitors.Add(new Competitor()
                {
                    FirstName = "Adam",
                    LastName = "Scott",
                    Nationality = "AUS",
                    Score = -7,
                    TotalStroke = 71,
                    SponsorId = 4
                });
                context.Competitors.Add(new Competitor()
                {
                    FirstName = "Brooks",
                    LastName = "Koepka",
                    Nationality = "USA",
                    Score = -7,
                    TotalStroke = 71,
                    SponsorId = 1
                });
                context.Competitors.Add(new Competitor()
                {
                    FirstName = "Dustin",
                    LastName = "Johnson",
                    Nationality = "USA",
                    Score = -6,
                    TotalStroke = 70,
                    SponsorId = 3
                }); 
                context.Competitors.Add(new Competitor()
                {
                    FirstName = "Louis",
                    LastName = "Oosthuizen",
                    Nationality = "RSA",
                    Score = -7,
                    TotalStroke = 66,
                    SponsorId = 1
                });
                context.Competitors.Add(new Competitor()
                {
                    FirstName = "Francesco",
                    LastName = "Molinari",
                    Nationality = "ITA",
                    Score = -7,
                    TotalStroke = 67,
                    SponsorId = 2
                });

                context.SaveChanges();
            }
        }

        public static void SeedChoaches()
        {
            Console.WriteLine("Seeding choaches...");

            using (CompetitionManagerDBContext context = new CompetitionManagerDBContext())
            {
                context.Coaches.Add(new Coach()
                {
                    FirstName = "Phillip",
                    LastName = "Allen",
                    CompetitorId = 1
                });
                context.Coaches.Add(new Coach()
                {
                    FirstName = "Anthony",
                    LastName = "Long",
                    CompetitorId = 2
                });
                context.Coaches.Add(new Coach()
                {
                    FirstName = "Russell",
                    LastName = "Richardson",
                    CompetitorId = 3
                });
                context.Coaches.Add(new Coach()
                {
                    FirstName = "Albert",
                    LastName = "Sanders",
                    CompetitorId = 4
                });
                context.Coaches.Add(new Coach()
                {
                    FirstName = "Johnny",
                    LastName = "Lewis",
                    CompetitorId = 5
                });
                context.Coaches.Add(new Coach()
                {
                    FirstName = "Samuel",
                    LastName = "Perez",
                    CompetitorId = 6
                });
                context.Coaches.Add(new Coach()
                {
                    FirstName = "Clarence",
                    LastName = "Butler",
                    CompetitorId = 7
                });
                context.Coaches.Add(new Coach()
                {
                    FirstName = "Randy",
                    LastName = "Johnson",
                    CompetitorId = 8
                });
                context.Coaches.Add(new Coach()
                {
                    FirstName = "Jonathan",
                    LastName = "Stewart",
                    CompetitorId = 9
                });

                context.SaveChanges();
            }
        }

        public static void SeedSponsors()
        {
            Console.WriteLine("Seeding sponsors...");

            using (CompetitionManagerDBContext context = new CompetitionManagerDBContext())
            {
                context.Sponsors.Add(new Sponsor() 
                { 
                    Brand = "Nike",
                    AnualPayLoad = 55000.00
                });
                context.Sponsors.Add(new Sponsor()
                {
                    Brand = "Callaway",
                    AnualPayLoad = 40000.00
                });
                context.Sponsors.Add(new Sponsor()
                {
                    Brand = "Adidas",
                    AnualPayLoad = 50000.00
                });
                context.Sponsors.Add(new Sponsor()
                {
                    Brand = "Titleist",
                    AnualPayLoad = 45000.00
                });

                
                context.SaveChanges();

                var sponsors = context.Sponsors.Include(s => s.Competitors).ToList();
            }
        }

        public static void SeedSkills()
        {
            Console.WriteLine("Seeding skills...");

            using (CompetitionManagerDBContext context = new CompetitionManagerDBContext())
            {

                context.Skills.Add(new Skill() 
                { 
                    Description = "Long hitter"
                });
                context.Skills.Add(new Skill()
                {
                    Description = "Long putter"
                });
                context.Skills.Add(new Skill()
                {
                    Description = "Shortgame expert"
                });
                context.Skills.Add(new Skill()
                {
                    Description = "Bunker expert"
                });

                context.SaveChanges();
            }
        }

        public static void SeedCompetitorSkills()
        {
            Console.WriteLine("Seeding competitor skills...");

            using (CompetitionManagerDBContext context = new CompetitionManagerDBContext())
            {
                context.CompetitorSkills.Add(new CompetitorSkill() 
                {
                    CompetitorId = 1,
                    SkillId = 1
                });
                context.CompetitorSkills.Add(new CompetitorSkill()
                {
                    CompetitorId = 1,
                    SkillId = 3
                });
                context.CompetitorSkills.Add(new CompetitorSkill()
                {
                    CompetitorId = 1,
                    SkillId = 4
                });
                context.CompetitorSkills.Add(new CompetitorSkill()
                {
                    CompetitorId = 2,
                    SkillId = 1
                });
                context.CompetitorSkills.Add(new CompetitorSkill()
                {
                    CompetitorId = 2,
                    SkillId = 2
                });
                context.CompetitorSkills.Add(new CompetitorSkill()
                {
                    CompetitorId = 3,
                    SkillId = 2
                });
                context.CompetitorSkills.Add(new CompetitorSkill()
                {
                    CompetitorId = 4,
                    SkillId = 1
                });
                context.CompetitorSkills.Add(new CompetitorSkill()
                {
                    CompetitorId = 5,
                    SkillId = 2
                });
                context.CompetitorSkills.Add(new CompetitorSkill()
                {
                    CompetitorId = 5,
                    SkillId = 4
                });
                context.CompetitorSkills.Add(new CompetitorSkill()
                {
                    CompetitorId = 6,
                    SkillId = 1
                });
                context.CompetitorSkills.Add(new CompetitorSkill()
                {
                    CompetitorId = 7,
                    SkillId = 1
                });
                context.CompetitorSkills.Add(new CompetitorSkill()
                {
                    CompetitorId = 8,
                    SkillId = 3
                });
                context.CompetitorSkills.Add(new CompetitorSkill()
                {
                    CompetitorId = 9,
                    SkillId = 3
                });

                context.SaveChanges();
            }
        }

        public static void EagerLoading()
        {
            Console.WriteLine("Eager loading...");

            using (CompetitionManagerDBContext context = new CompetitionManagerDBContext())
            {
                var competitors = context.Competitors.Include(c => c.Sponsor).ThenInclude(s => s.Competitors).ToList();
                var coaches = context.Coaches.Include(c => c.Competitor).ToList();
                var sponsors = context.Sponsors.Include(s => s.Competitors).ToList();

                var competitor = competitors[0];
                var competitorSkill = context.CompetitorSkills.Where(cs => cs.CompetitorId == competitor.Id).Include(cs => cs.Skill).ToList();

                Console.WriteLine();
                Console.WriteLine($"{competitor.FirstName} {competitor.LastName} has {competitor.Sponsor.Brand} " +
                    $"sponsorship, {competitor.Coach.FirstName} {competitor.Coach.LastName} as coach and has skills: ");

                foreach (CompetitorSkill skill in competitorSkill)
                {
                    Console.WriteLine($"- {skill.Skill.Description}");
                }

                Console.WriteLine();
            }
        }

        public static void ReadCompetitors()
        {
            using (CompetitionManagerDBContext context = new CompetitionManagerDBContext())
            {
                List<Competitor> allCompetitors = context.Competitors.ToList();

                Console.WriteLine("======= Competitors =======");

                foreach (Competitor competitor in context.Competitors)
                {
                    Console.WriteLine();
                    Console.WriteLine($"Competitor name: {competitor.FirstName} {competitor.LastName}");
                    Console.WriteLine($"Nationality: {competitor.Nationality}");
                    Console.WriteLine($"Score: {competitor.Score}");
                    Console.WriteLine($"Today: {competitor.TotalStroke}");
                }
            }
        }
    }
}
